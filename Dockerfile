FROM  openjdk:11-jre-slim

COPY target/ESportApp-1.0.jar .

CMD [ "java", "-jar", "ESportApp-1.0.jar" ]