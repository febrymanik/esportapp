# ESportApp

Simple project for esport team

## Getting Started


### Dependencies

* maven (springboot)
* jdk-jre
* docker
* database (mysql)
* swagger


### Setup


#### Build Maven Project

```
    $ mvn clean package -DskipTest
```
#### Build Docker image

```
    $ cd [docker file location]
    $ docker build --tag [imagename]:[tag] .
```

#### Docker compose

```
    $ cd [docker-compose file location]
    $ docker-compose up -d 
```

## Authors

Febry Saputra Manik [@febrymanik](https://gitlab.com/febrymanik)

## Version History

* 1.0
    * init project


## Acknowledgments

* Swagger location : http://localhost:8080/swagger-ui.html
