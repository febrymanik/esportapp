package com.feb.dota.EsportApp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.feb.dota.EsportApp.model.Team;
import com.feb.dota.EsportApp.modeltemp.TeamObjectV1;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long>{

	
	@Query(value = "Select * from esport.team where name = :name" , nativeQuery = true)
	Team getByName(String name);
	
	//example for join
	@Query(
			value = "Select t.id, t.name , t.regional , t.status , count(p.name) as totalActivePlayer from "
					+ "team t left join player p "
					+ "on t.id = p.team_id "
					+ "where upper(p.status) = 'ACTIVE' "
					+ "group by t.name "
					+ "having count(p.id) > 0 "
					+ "order by t.name desc"
			,nativeQuery = true)
	List<TeamObjectV1> getTotalTeamWithAcvitePlayer();

	
	//examnple sub query
	@Query(
			value ="Select * from "
					+ "("
						+ "Select t.id, t.name , t.regional , t.status,  count(p.id) as totalActivePlayer from "
						+ "player p left join team t "
						+ "on p.team_id = t.id "
						+ "where t.status = 'ACTIVE' and p.status = 'ACTIVE' "
						+ "group by t.name "
						+ "having count(p.id) "
						+ "order by t.id "
					+ ") temp "
					+ "where id in "
					+ "( "
						+ "Select t.id from "
						+ "player p left join team t "
						+ "on p.team_id  = t.id "
						+ "where p.country like 'I_____%' "
						+ "and p.status in ('ACTIVE', 'INACTIVE') "
						+ "group by t.id "
						+ "having count(t.id) > 1 "
					+ ")" 
			,nativeQuery = true)
	List<TeamObjectV1> getTeamWithPlayerActiveAndCountCountryFiltered();
	
}
