package com.feb.dota.EsportApp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.feb.dota.EsportApp.model.Player;
import com.feb.dota.EsportApp.modeltemp.PlayerObjectV1;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {

	@Query(value = "Select * from esport.player where name = :name", nativeQuery = true)
	Player getByName(String name);

	@Query(
			value = "Call getAllPLayer(:ageConstraint)"
			,nativeQuery = true)
	List<PlayerObjectV1> getPlayerWithAge(Integer ageConstraint);
}
