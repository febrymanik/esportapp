package com.feb.dota.EsportApp.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.feb.dota.EsportApp.enums.TeamStatus;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table
public class Team {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(hidden = true)
	private Long id;

	@Column(unique = true)
	private String name;

	private String regional;

	@Column(length = 10)
	@Enumerated(EnumType.STRING)
	private TeamStatus status;

	@OneToMany(targetEntity = Player.class, mappedBy = "team")
	private Set<Player> member;

	public Team(String name, String regional, TeamStatus status) {
		this.name = name;
		this.regional = regional;
		this.status = status;
	}

	public Team() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegional() {
		return regional;
	}

	public void setRegional(String regional) {
		this.regional = regional;
	}

	public TeamStatus getStatus() {
		return status;
	}

	public void setStatus(TeamStatus status) {
		this.status = status;
	}

}
