package com.feb.dota.EsportApp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.feb.dota.EsportApp.enums.PlayerStatus;
import com.feb.dota.EsportApp.enums.Role;

import io.swagger.annotations.ApiModelProperty;

@Entity(name = "Player")
@Table
public class Player {

	@Id
	/*
	 * @SequenceGenerator( name = "sequence_sequence", sequenceName =
	 * "sequence_sequence", allocationSize = 1 )
	 * 
	 * @GeneratedValue( strategy = GenerationType.SEQUENCE, generator =
	 * "team_sequence" )
	 */
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(hidden = true)
	private Long id;

	@Column(unique = true)
	private String name;

	@Column(name = "birth_date")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta", shape = JsonFormat.Shape.STRING)
	private Date birthDate;

	private String country;

	@Column(length = 10)
	@Enumerated(EnumType.STRING)
	private PlayerStatus status;

	@Column(length = 25)
	@Enumerated(EnumType.STRING)
	private Role role;

	@ManyToOne
	@JoinColumn(name = "team_id", referencedColumnName = "id")
	@JsonIgnore
	private Team team;

	@Transient
	private String teamName;

	public Player(String name, Date birthDate, String country, PlayerStatus status, Role role) {
		this.name = name;
		this.birthDate = birthDate;
		this.country = country;
		this.status = status;
		this.role = role;
	}

	public Player() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public PlayerStatus getStatus() {
		return status;
	}

	public void setStatus(PlayerStatus status) {
		this.status = status;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	@Override
	public String toString() {
		return "Player [id=" + id + ", name=" + name + ", birthDate=" + birthDate + ", country=" + country + ", status="
				+ status + ", role=" + role + ", team=" + team + ", teamName=" + teamName + "]";
	}

}
