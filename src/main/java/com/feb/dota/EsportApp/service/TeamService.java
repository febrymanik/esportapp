package com.feb.dota.EsportApp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.feb.dota.EsportApp.model.Team;
import com.feb.dota.EsportApp.modeltemp.TeamObjectV1;
import com.feb.dota.EsportApp.repository.TeamRepository;

@Service
public class TeamService {

	@Autowired
	private TeamRepository teamRepo;

	public List<Team> getAllTeam() {
		List<Team> teams = new ArrayList<Team>();
		try {
			teams = teamRepo.findAll();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return teams;
	}

	public boolean saveTeam(Team team) {

		try {
			teamRepo.save(team);

			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public List<TeamObjectV1> getTotalActivePlayer() {

		List<TeamObjectV1> teamActivePlayer = new ArrayList<>();
		try {

			teamActivePlayer = teamRepo.getTotalTeamWithAcvitePlayer();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teamActivePlayer;
	}

	public List<TeamObjectV1> getTeamCostumFilter() {
		List<TeamObjectV1> teamActivePlayer = new ArrayList<>();
		try {

			teamActivePlayer = teamRepo.getTeamWithPlayerActiveAndCountCountryFiltered();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teamActivePlayer;
	}

}
