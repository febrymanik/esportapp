package com.feb.dota.EsportApp.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.feb.dota.EsportApp.enums.PlayerStatus;
import com.feb.dota.EsportApp.model.Player;
import com.feb.dota.EsportApp.model.Team;
import com.feb.dota.EsportApp.modeltemp.PlayerObjectV1;
import com.feb.dota.EsportApp.repository.PlayerRepository;
import com.feb.dota.EsportApp.repository.TeamRepository;
import com.feb.dota.EsportApp.util.PlayerUtil;

@Service
public class PlayerService {

	@Autowired
	private PlayerRepository playerRepo;

	@Autowired
	private TeamRepository teamRepo;

	public List<Player> getAllPlayer() {
		List<Player> players = new ArrayList<Player>();
		try {
			players = playerRepo.findAll();

			players.stream().map(player -> {
				return PlayerUtil.constTeamName(player);
			}).collect(Collectors.toList());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return players;
	}

	public Player getPlayerByName(String name) {

		return playerRepo.getByName(name);
	}

	public boolean savePlayer(Player player) {
		try {
			if (player.getTeamName() != null) {
				Team team = teamRepo.getByName(player.getTeamName());

				if (team != null) {
					player.setTeam(player.getStatus().equals(PlayerStatus.RETIRED) ? null : team);
				}
			}

			playerRepo.save(player);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public Map<String, List<Player>> getPlayerGroupByStatus() {

		Map<String, List<Player>> playersGroup = new HashMap<>();
		try {
			final List<Player> players = playerRepo.findAll();

			players.stream().map(player -> {
				return PlayerUtil.constTeamName(player);
			}).collect(Collectors.toList());

			Arrays.stream(PlayerStatus.values()).forEach(e -> {
				playersGroup.put(e.name(),
						players.stream().filter(player -> player.getStatus().equals(e)).collect(Collectors.toList()));
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
		return playersGroup;
	}

	public List<PlayerObjectV1> getAllPlayerWithAgeOver(Integer ageConstraint) {
		List<PlayerObjectV1> players = new ArrayList<>();
		try {
			players = playerRepo.getPlayerWithAge(ageConstraint);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return players;
	}

}
