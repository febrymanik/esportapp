package com.feb.dota.EsportApp.modeltemp;

public interface TeamObjectV1 {

	Long getId();

	String getName();

	String getRegional();

	String getStatus();

	Integer getTotalActivePlayer();

}
