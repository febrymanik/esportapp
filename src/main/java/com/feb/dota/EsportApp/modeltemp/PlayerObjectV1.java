package com.feb.dota.EsportApp.modeltemp;

import java.util.Date;

public interface PlayerObjectV1 {

	Long getId();

	String getCountry();

	String getName();

	String getRole();

	String getStatus();

	String getBirthDate();
	
	Integer getAge();

}
