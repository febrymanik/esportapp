package com.feb.dota.EsportApp.enums;

public enum PlayerStatus {
	ACTIVE,
	INACTIVE,
	RETIRED,
}
