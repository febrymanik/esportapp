package com.feb.dota.EsportApp.enums;

public enum Role {
	CARRY, 
	MID, 
	OFFLANER, 
	SOFT_SUPPORT, 
	HARD_SUPPORT;
}
