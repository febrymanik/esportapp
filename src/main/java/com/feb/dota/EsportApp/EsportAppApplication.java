package com.feb.dota.EsportApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsportAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsportAppApplication.class, args);
	}

}
