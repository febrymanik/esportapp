package com.feb.dota.EsportApp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.feb.dota.EsportApp.model.Team;
import com.feb.dota.EsportApp.modeltemp.TeamObjectV1;
import com.feb.dota.EsportApp.service.TeamService;
import com.feb.dota.EsportApp.util.EnumChecker;

@RestController
@RequestMapping("api/v1/teams")
public class TeamController {

	@Autowired
	private TeamService teamService;

	@GetMapping
	public ResponseEntity<List<Team>> findAll() {
		List<Team> teams = teamService.getAllTeam();

		if (!teams.isEmpty()) {

			return new ResponseEntity<List<Team>>(teams, HttpStatus.FOUND);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PostMapping
	public ResponseEntity<?> saveTeam(@RequestBody Team team) {

		if (team != null) {

			if (!EnumChecker.isTeamStatusEnum.apply(team.getStatus()))
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

			if (teamService.saveTeam(team)) {
				return new ResponseEntity<>(HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	@GetMapping("/total-active")
	public ResponseEntity<?> getTotalActiveTeamPlayer() {
		List<TeamObjectV1> teams = teamService.getTotalActivePlayer();

		if (!teams.isEmpty()) {

			return new ResponseEntity<>(teams, HttpStatus.FOUND);
		}

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);

	}

	@GetMapping("/costum-filtered")
	public ResponseEntity<?> getTeamCostumFiltered() {
		List<TeamObjectV1> teams = teamService.getTeamCostumFilter();

		if (!teams.isEmpty()) {

			return new ResponseEntity<>(teams, HttpStatus.FOUND);
		}

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
