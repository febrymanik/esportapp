package com.feb.dota.EsportApp.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.feb.dota.EsportApp.model.Player;
import com.feb.dota.EsportApp.modeltemp.PlayerObjectV1;
import com.feb.dota.EsportApp.service.PlayerService;
import com.feb.dota.EsportApp.util.EnumChecker;
import com.feb.dota.EsportApp.util.PlayerUtil;

@RestController
@RequestMapping("api/v1/players")
public class PlayerController {

	@Autowired
	private PlayerService playerService;

	@GetMapping
	public ResponseEntity<List<Player>> findAll() {

		List<Player> players = playerService.getAllPlayer();

		if (!players.isEmpty()) {

			return new ResponseEntity<List<Player>>(players, HttpStatus.FOUND);
		}
		return new ResponseEntity<>(new ArrayList<Player>(), HttpStatus.NO_CONTENT);
	}

	@GetMapping(path = "{name}")
	public ResponseEntity<Player> findByName(@PathVariable("name") String name) {
		Player player = playerService.getPlayerByName(name);

		if (player != null) {
			player = PlayerUtil.constTeamName(player);
			return new ResponseEntity<Player>(player, HttpStatus.FOUND);
		}

		return new ResponseEntity<Player>(HttpStatus.NOT_FOUND);
	}

	@PostMapping
	public ResponseEntity<?> savePlayer(@RequestBody Player player) {
		if (player != null) {

			if (!EnumChecker.isRoleEnum.apply(player.getRole()))
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

			if (!EnumChecker.isPlayerStatusEnum.apply(player.getStatus()))
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

			if (playerService.savePlayer(player)) {
				return new ResponseEntity<>(HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	@GetMapping("status-group")
	public ResponseEntity<?> findPlayerGroupStatus() {

		Map<String, List<Player>> groups = playerService.getPlayerGroupByStatus();

		return new ResponseEntity<>(groups, HttpStatus.OK);

	}
	
	@GetMapping("filter")
	public ResponseEntity<?> findPlayerWithAgeOver(@RequestParam("age") Integer ageConstraint){
		
		List<PlayerObjectV1> players = playerService.getAllPlayerWithAgeOver(ageConstraint);

		if (!players.isEmpty()) {

			return new ResponseEntity<>(players, HttpStatus.FOUND);
		}

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
