package com.feb.dota.EsportApp.util;

import com.feb.dota.EsportApp.enums.PlayerStatus;
import com.feb.dota.EsportApp.model.Player;

public class PlayerUtil {

	public static Player constTeamName(Player player) {
		if (player.getTeam() != null) {
			player.setTeamName(player.getTeam().getName());
		} else {
			player.setTeamName(
					player.getStatus().equals(PlayerStatus.RETIRED) ? "PLayer Already Retired" : "No Team registered");
		}

		return player;
	}
}
