package com.feb.dota.EsportApp.util;

import java.util.function.Function;
import java.util.stream.Stream;

import com.feb.dota.EsportApp.enums.PlayerStatus;
import com.feb.dota.EsportApp.enums.Role;
import com.feb.dota.EsportApp.enums.TeamStatus;

public class EnumChecker {

	public static Function<Role, Boolean> isRoleEnum = (en) -> {
		return Stream.of(Role.values()).anyMatch(e -> e.equals(en));
	};

	public static Function<PlayerStatus, Boolean> isPlayerStatusEnum = (en) -> {
		return Stream.of(PlayerStatus.values()).anyMatch(e -> e.equals(en));
	};

	public static Function<TeamStatus, Boolean> isTeamStatusEnum = (en) -> {
		return Stream.of(TeamStatus.values()).anyMatch(e -> e.equals(en));
	};

}
