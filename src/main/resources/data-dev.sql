
/*
 * Init data for table team
 */

INSERT INTO esport.team(name, regional, status)
VALUES("OG", "Europe", "ACTIVE");

INSERT INTO esport.team(name, regional, status)
VALUES("BOOM Esport", " Southeast Asia", "ACTIVE");

INSERT INTO esport.team(name, regional, status)
VALUES("Team Secret", "Europe", "ACTIVE");



/*
 * Init data for  table player
 */
INSERT INTO esport.player
(birth_date, country, name, `role`, status, team_id)
VALUES("2020-02-09", "Indonesia", "ana", "CARRY", "ACTIVE", 1);

INSERT INTO esport.player
(birth_date, country, name, `role`, status, team_id)
VALUES("2020-02-09", "FINLAND", "topson", "MID", "ACTIVE", 1);

INSERT INTO esport.player
(birth_date, country, name, `role`, status, team_id)
VALUES("2020-02-09", "France", "ceb", "OFFLANER", "ACTIVE", 1);

INSERT INTO esport.player
(birth_date, country, name, `role`, status, team_id)
VALUES("2020-02-09", "North Macedonia", "saksa", "SOFT_SUPPORT", "ACTIVE", 1);

INSERT INTO esport.player
(birth_date, country, name, `role`, status, team_id)
VALUES("2020-02-09", "Denmark", "nOtail", "HARD_SUPPORT", "ACTIVE", 1);


INSERT INTO esport.player
(birth_date, country, name, `role`, status, team_id)
VALUES("2020-02-09", "Indonesia", "A", "HARD_SUPPORT", "ACTIVE", 2);


INSERT INTO esport.player
(birth_date, country, name, `role`, status, team_id)
VALUES("2020-02-09", "Indonesia", "B", "HARD_SUPPORT", "ACTIVE", 2);


INSERT INTO esport.player
(birth_date, country, name, `role`, status, team_id)
VALUES("2020-02-09", "Irlandia", "C", "HARD_SUPPORT", "INACTIVE", 2);



INSERT INTO esport.player
(birth_date, country, name, `role`, status, team_id)
VALUES("2020-02-09", "India", "D", "HARD_SUPPORT", "ACTIVE", 2);


INSERT INTO esport.player
(birth_date, country, name, `role`, status, team_id)
VALUES("2020-02-09", "Denmark", "e", "HARD_SUPPORT", "ACTIVE", 2);


INSERT INTO esport.player
(birth_date, country, name, `role`, status, team_id)
VALUES("2020-02-09", "India", "F", "HARD_SUPPORT", "ACTIVE", 3);


INSERT INTO esport.player
(birth_date, country, name, `role`, status, team_id)
VALUES("2020-02-09", "Israel", "G", "HARD_SUPPORT", "ACTIVE", 3);


INSERT INTO esport.player
(birth_date, country, name, `role`, status, team_id)
VALUES("2020-02-09", "Israel", "H", "HARD_SUPPORT", "ACTIVE", 3);



INSERT INTO esport.player
(birth_date, country, name, `role`, status, team_id)
VALUES("2020-02-09", "Italy", "I", "HARD_SUPPORT", "ACTIVE", 3);


INSERT INTO esport.player
(birth_date, country, name, `role`, status, team_id)
VALUES("2020-02-09", "Iran", "J", "HARD_SUPPORT", "ACTIVE", 3);



